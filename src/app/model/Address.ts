import {IAddress} from "./IAddress";


export class Address implements IAddress {

  public readonly ipPattern = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g

  ip: string;
  mask?: IAddress;

  constructor(ip: string, mask?: number) {
    this.ip = ip
    if (mask) {
      if (mask == -1) this.mask = this.getDefaultMask()
      else
        this.mask = Address.generateMask(mask)

    }
  }


  getDefaultMask(): IAddress {
    let addressClass = this.getIpDefaultClass()
    switch (addressClass) {
      case 'A':
        return Address.generateMask(8)
      case 'B':
        return Address.generateMask(16)
      case 'C':
        return Address.generateMask(24)
      case 'D':
        return Address.generateMask(32)
      default:
        return new Address("", 0)
    }
  }

  getIpArray(): number[] {
    let ipArray: number[] = []
    let split = this.ip.split('.')
    for (let string of split) {
      ipArray.push(parseInt(string))
    }

    return ipArray;
  }

  getIpBinary(): string {
    let split = this.ip.trim().split('.')
    if (split) {
      // @ts-ignore
      let g1 = (split[0] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g2 = (split[1] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g3 = (split[2] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g4 = (split[3] >>> 0).toString(2).padStart(8, '0')

      return g1.concat(g2, g3, g4)
    }
    return "";
  }

  getIpDefaultClass(): string {
    let firstOctet = this.getIpArray()[0]
    if (firstOctet <= 126) {
      return 'A'
    } else if (firstOctet <= 191) {
      return 'B'
    } else if (firstOctet <= 223) {
      return 'C'
    } else if (firstOctet <= 239) {
      return 'D'
    }
    return "E";
  }

  getSuperNet(): IAddress {
    let binMask = this.mask!.getIpBinary()
    let bin = this.getIpBinary()
    let netAddress = ""
    for (let i = 0; i < binMask.length; i++) {
      if (bin[i] == binMask[i]) netAddress += bin[i]
      else netAddress += "0"
    }
    return new Address(Address.ipFromBinary(netAddress), -1);
  }

  public static generateMask(number: number) {

    return new Address(this.ipFromBinary("1".repeat(number).padEnd(32, '0')));
  }

  public static ipFromBinary(binaryAddress: string): string {
    let g1 = parseInt(binaryAddress.slice(0, 8), 2)
    let g2 = parseInt(binaryAddress.slice(8, 16), 2)
    let g3 = parseInt(binaryAddress.slice(16, 24), 2)
    let g4 = parseInt(binaryAddress.slice(24, 32), 2)
    return g1 + '.' + g2 + '.' + g3 + '.' + g4
  }

  getShort(): number {
    return this.getIpBinary().lastIndexOf('1') + 1
  }

  toString(): string {
    return this.getIpArray().join('.')
  }
}
