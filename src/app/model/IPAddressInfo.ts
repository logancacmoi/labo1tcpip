export class IPAddressInfo {


  public readonly ipPattern = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/g

  private _address: string

  set address(value: string) {
    this._address = value
  }

  get address() {
    return this._address
  }

  private _mask: number
  get mask(): number {
    return this._mask;
  }

  set mask(value: number) {
    this._mask = value;
  }


  constructor(address: string, mask: number) {
    this._address = address
    this._mask = mask
  }

  get networkAddress() {
    let binMask = this.getBinaryMask()
    let binAddress = this.getBinary()

    if (binAddress != null && binMask != null) {
      let netAddress =""
      for (let i = 0; i < binMask.length; i++) {
        if (binAddress[i] == binMask[i]) netAddress+=binAddress[i]
        else netAddress+="0"
      }
      return netAddress
    }
    return null
  }

  private addressValidation(addressToTest: string): boolean {
    return this.ipPattern.test(addressToTest)
  }

  public getBinary(): string | null {
    let split = this._address.trim().split(this.ipPattern).slice(1, 5)
    if (split) {
      // @ts-ignore
      let g1 = (split[0] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g2 = (split[1] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g3 = (split[2] >>> 0).toString(2).padStart(8, '0')
      // @ts-ignore
      let g4 = (split[3] >>> 0).toString(2).padStart(8, '0')

      let result = g1.concat(g2, g3, g4)
      return result
    }
    return null
  }

  public static formatBinaryAddress(binaryAddress: string): string {

    let g1 = parseInt(binaryAddress.slice(0, 8), 2)
    let g2 = parseInt(binaryAddress.slice(8, 16), 2)
    let g3 = parseInt(binaryAddress.slice(16, 24), 2)
    let g4 = parseInt(binaryAddress.slice(24, 32), 2)
    return g1 + '.' + g2 + '.' + g3 + '.' + g4
  }

  public getFormattedMask(): string | null {
    let binMask = this.getBinaryMask()
    if (binMask)

      return IPAddressInfo.formatBinaryAddress(binMask)
    return null
  }

  public getBinaryMask(): string | null {
    let shortMask = this.mask;
    if (!isNaN(shortMask)) {

      return ('1'.repeat(shortMask)).padEnd(32, '0')
    }
    return null
  }

  public getClass(): string | null {
    let bin = this.getBinaryMask()
    if (bin) {
      if (bin.slice(0, 3) === "1111") {
        return 'E'
      } else if (bin.slice(0, 3) === "111") {
        return 'D'
      } else if (bin.slice(0, 2) === "11") {
        return 'C'
      } else if (bin.slice(0, 1) === "1") {
        return 'B'
      } else return 'A'
    }
    return null
  }

  getNbHost() {
    let binMask = this.getBinaryMask()
    if (binMask) {
      let freeBits = (binMask.match(/0/g) || []).length
      return Math.pow(freeBits, 2)
    }
    return -1
  }


}
