import {INetwork} from "./INetwork";
import {IAddress} from "./IAddress";
import {Address} from "./Address";

export class Network implements INetwork {
  address: IAddress;
  mask: IAddress;


  constructor(address: IAddress, mask: IAddress) {
    this.address = address
    this.mask = mask
  }

  contains(address: IAddress): boolean {
    return this.addresses.find(value => value.ip == address.ip) != null
  }


  get hosts(): number {
    let freeBits: number = (32 - this.mask.getShort())
    return Math.pow(2, freeBits);
  }

  get addresses(): IAddress[] {
    let array: IAddress[] = []
    for (let i = 0; i < this.hosts; i++) {
      array.push(this.getNextAddress(i))
    }
    return array
  }


  private getNextAddress(bound: number): IAddress {
    if (bound == 0) return this.address
    let ipOctets = this.address.getIpArray()
    ipOctets[ipOctets.length - 1] = ipOctets[ipOctets.length - 1] += bound


    let rest = 0
    for (let i = ipOctets.length - 1; i > 0; i--) {
      ipOctets[i] = ipOctets[i] += rest
      rest = 0
      if (ipOctets[i] > 255) {
        rest = Math.floor(ipOctets[i] / 255)
        ipOctets[i] = ipOctets[i] % 255
      }
    }

    return new Address(ipOctets.join('.'), this.address.mask?.getShort())

  }

  get broadcast(): IAddress {
    return this.addresses[this.addresses.length - 1]
  }

  get shortMask(): number {
    return this.mask.getShort()
  }

  getSubnets(submask: number): INetwork[] {
    if (submask > this.mask.getShort()) {
      let subnets = []
      let hostsForMask = Math.pow(32 - submask, 2)
      let addresses = this.addresses
      console.log(addresses.length)
      let pos = 0
      do {
        subnets.push(new Network(addresses[pos], Address.generateMask(submask)))
        pos += hostsForMask
      } while (pos < addresses.length)
      return subnets
    }

    return [];
  }

  lastUsableHost(): IAddress {
    return this.addresses[this.addresses.length - 2];
  }

  firstUsableHost(): IAddress {
    return this.addresses[1];
  }

  getUsableHosts(): IAddress[] {
    return this.addresses.slice(1, this.hosts - 1)
  }
}
