export interface IAddress {

  ip: string
  mask?: IAddress

  //ipClass: string

  getIpArray(): number[]

  getIpBinary(): string

  getIpDefaultClass(): string

  getDefaultMask(): IAddress

  getSuperNet(): IAddress

  getShort(): number

  toString(): string


}
