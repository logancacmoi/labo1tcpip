import {IAddress} from "./IAddress";

export interface INetwork {
  addresses: IAddress[]
  address: IAddress
  broadcast: IAddress
  hosts: number
  shortMask: number
  mask: IAddress

  getSubnets(num: number): INetwork[]

  lastUsableHost(): IAddress;

  firstUsableHost(): IAddress;

  contains(address: IAddress): boolean;

  getUsableHosts(): IAddress[]
}
