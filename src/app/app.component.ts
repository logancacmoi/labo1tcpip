import {Component} from '@angular/core';
import {IPaddressService} from "./ipaddress.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'labo1App';

  constructor(public iPaddressService: IPaddressService) {
  }

}



