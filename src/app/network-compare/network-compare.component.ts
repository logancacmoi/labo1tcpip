import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {IAddress} from "../model/IAddress";
import {Address} from "../model/Address";
import {Network} from "../model/Network";

@Component({
  selector: 'app-network-compare',
  templateUrl: './network-compare.component.html',
  styleUrls: ['./network-compare.component.css']
})
export class NetworkCompareComponent implements OnInit {

  ipRegex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  maskRegex = /^([1-9]|[12][0-9]|3[012])$/


  addressCompareForm = new FormBuilder().group({
    address1: new FormControl("", [Validators.required, Validators.pattern(this.ipRegex)]),
    address2: new FormControl("", [Validators.required, Validators.pattern(this.ipRegex)]),
    mask1: new FormControl("", [Validators.required, Validators.pattern(this.maskRegex)]),
    mask2: new FormControl("", [Validators.required, Validators.pattern(this.maskRegex)])
  });

  address1?: IAddress
  address2?: IAddress
  address1InAddress2: boolean = false
  address2InAddress1: boolean = false


  constructor(private formGroup: FormBuilder) {
  }

  ngOnInit(): void {
  }

  get address1Input() {
    return this.addressCompareForm.get("address1")
  }

  get address2Input() {
    return this.addressCompareForm.get("address2")
  }

  get mask1Input() {
    return this.addressCompareForm.get("mask1")
  }

  get mask2Input() {
    return this.addressCompareForm.get("mask2")
  }

  validate() {
    this.address1 = undefined
    this.address2 = undefined
    this.address2InAddress1=false
    this.address1InAddress2=false

    if (this.addressCompareForm.valid) {
      this.address1 = new Address(this.address1Input?.value, this.mask1Input?.value)
      this.address2 = new Address(this.address2Input?.value, this.mask2Input?.value)

      this.address1InAddress2 = (new Network(this.address2.getSuperNet(), this.address2.mask!).contains(this.address1))
      this.address2InAddress1 = (new Network(this.address1.getSuperNet(), this.address1.mask!).contains(this.address2))

    }
  }

}
