import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkCompareComponent } from './network-compare.component';

describe('NetworkCompareComponent', () => {
  let component: NetworkCompareComponent;
  let fixture: ComponentFixture<NetworkCompareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NetworkCompareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
