import { TestBed } from '@angular/core/testing';

import { IPaddressService } from './ipaddress.service';

describe('IPaddressService', () => {
  let service: IPaddressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IPaddressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
