import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomeComponent} from './home/home/home.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AddressInfoComponent} from './address-info/address-info.component';
import {NetworkComponent} from './network-info/network/network.component';
import {AddressCompareComponent} from './addressCompare/address-compare/address-compare.component';
import {RouterModule, Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {root} from "rxjs/internal-compatibility";
import { NetworkCompareComponent } from './network-compare/network-compare.component';


const routes: Routes = [
  {path: 'address-info', component: AddressInfoComponent},
  {path: 'network-info', component: NetworkComponent},
  {path: 'compare-address-network', component: AddressCompareComponent},
  {path: 'compare-two-adresses', component: NetworkCompareComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AddressInfoComponent,
    NetworkComponent,
    AddressCompareComponent,
    NetworkCompareComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    RouterTestingModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
