import {Component, OnInit} from '@angular/core';
import {IPaddressService} from "../../ipaddress.service";
import {INetwork} from "../../model/INetwork";
import {AbstractControl, FormBuilder, FormControl, Validators} from "@angular/forms";
import {Address} from "../../model/Address";
import {Network} from "../../model/Network";

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.css']
})
export class NetworkComponent implements OnInit {


  ipRegex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  maskRegex = /^([1-9]|[12][0-9]|3[012])$/
  ipAddress: string = "";
  shortMaskInput: number = 30;


  addressForm = this.formBuilder.group({
    address: new FormControl(this.ipAddress, [Validators.pattern(this.ipRegex), Validators.required]),
    mask: new FormControl(this.shortMaskInput, [Validators.pattern(this.maskRegex), Validators.required]),
  });

  network?: INetwork = this.iPaddressService?.network
  subnetForm = this.formBuilder.group({
      subnetShort: new FormControl(Validators.pattern(/^([1-9]|[12][0-9]|3[012])/))
    }
  );


  constructor(public iPaddressService: IPaddressService, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {

  }


  subnet() {
    this.network?.getSubnets(this.inputSubMask?.value)
  }

  validSubnetting(): boolean {
    let mask = this.inputSubMask!.value
    console.log(mask)
    if (mask > this.network!.mask.getShort()) {
      this.network?.getSubnets(mask)
      return true
    }
    return false
  }

  get inputSubMask() {
    return this.subnetForm.get("subnetShort")
  }

  getValidSubMask(): number[] {
    let array: number[] = []
    let mask = this.network!.mask.getShort() + 1
    do {
      array.push(mask)
      mask++
    } while (mask <= 30)

    return array

  }

  get inputAddress() {
    return this.addressForm.get('address')
  }

  get inputMask(): AbstractControl {
    return this.addressForm.get("mask")!
  }

  validate() {
    if (!this.iPaddressService.address) {
      this.iPaddressService.address = new Address("0.0.0.0")
    }
    if (!this.iPaddressService.network) {
      this.iPaddressService.network = new Network(this.iPaddressService.address, this.iPaddressService.address.mask!)
    }

    this.iPaddressService.address.ip = this.inputAddress?.value
    this.iPaddressService.address.mask = Address.generateMask(this.inputMask?.value)
    this.network = this.iPaddressService.network
    this.iPaddressService.network.address = this.iPaddressService.address.getSuperNet()
    this.iPaddressService.network.mask = this.iPaddressService.address.mask
  }
}
