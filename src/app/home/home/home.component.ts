import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormControl, Validators} from "@angular/forms";
import {IPaddressService} from "../../ipaddress.service";
import {Address} from "../../model/Address";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  ipRegex = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";
  maskRegex = "^([1-9]|[12][0-9]|3[012])$"

  ipAddress: string = "109.89.253.122";
  mask: number = 30


  addressForm = this.formBuilder.group({
    address: new FormControl(this.ipAddress, Validators.pattern(this.ipRegex)),
    mask: new FormControl(Validators.pattern(/^([1-9]|[12][0-9]|3[012])$/)),
    subMask: new FormControl(Validators.pattern(/^([1-9]|[12][0-9]|3[012])$/), Validators.min(this.mask))
  });

  get inputAddress() {
    return this.addressForm.get('address')
  }

  get inputMask(): AbstractControl {
    return this.addressForm.get("mask")!
  }

  get inputSubmask() {
    return this.addressForm.get("submask")
  }

  constructor(private formBuilder: FormBuilder, public iPaddressService: IPaddressService) {
  }

  ngOnInit(): void {
  }

  validate() {


  }


}
