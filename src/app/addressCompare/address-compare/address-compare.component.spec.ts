import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressCompareComponent } from './address-compare.component';

describe('AddressCompareComponent', () => {
  let component: AddressCompareComponent;
  let fixture: ComponentFixture<AddressCompareComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddressCompareComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressCompareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
