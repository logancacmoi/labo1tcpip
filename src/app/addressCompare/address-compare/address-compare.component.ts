import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Address} from "../../model/Address";
import {Network} from "../../model/Network";

@Component({
  selector: 'app-address-compare',
  templateUrl: './address-compare.component.html',
  styleUrls: ['./address-compare.component.css']
})
export class AddressCompareComponent implements OnInit {
  networkTestForm = this.formBuilder.group({
    baseAddress: new FormControl("", [Validators.required, Validators.pattern(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)]),
    baseMask: new FormControl(30, [Validators.required, Validators.pattern(/^([1-9]|[12][0-9]|3[012])$/)]),
    networkAddress: new FormControl("", [Validators.required, Validators.pattern(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?).(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)])
  });

  isInNetwork: boolean = false
  isUsableHost: boolean = false
  testAddress?: Address = undefined
  testNetwork?: Network = undefined

  constructor(private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
  }


  get inputBaseAddress() {
    return this.networkTestForm.get("baseAddress")
  }


  get inputBaseMask() {
    return this.networkTestForm.get("baseMask")
  }

  get inputNetworkAddress() {
    return this.networkTestForm.get("networkAddress")
  }

  submit() {
    this.testAddress = new Address(this.inputBaseAddress?.value, this.inputBaseMask?.value)
    this.testNetwork = new Network(new Address(this.inputNetworkAddress?.value), this.testAddress.mask!)
    this.isInNetwork = this.testNetwork.contains(this.testAddress);
    console.log(this.testNetwork.getUsableHosts())
    console.log(this.testAddress)
    this.isUsableHost = this.isInNetwork && (this.testNetwork.getUsableHosts().find(value => value.ip == this.testAddress!.ip) != null)
  }

  isBroadcast() {

  }
}
