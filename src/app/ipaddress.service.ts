import {Injectable} from '@angular/core';
import {IAddress} from "./model/IAddress";
import {INetwork} from "./model/INetwork";


@Injectable({
  providedIn: 'root'
})
export class IPaddressService {


  address?: IAddress = undefined
  network?: INetwork = undefined;


  get valid() {
    return this.address != null && this.network != null
  }


  constructor() {
  }
}
