import {Component, OnInit} from '@angular/core';
import {IAddress} from "../model/IAddress";
import {FormBuilder, FormControl, Validators} from "@angular/forms";
import {Address} from "../model/Address";

@Component({
  selector: 'app-address-info',
  templateUrl: './address-info.component.html',
  styleUrls: ['./address-info.component.css']
})
export class AddressInfoComponent implements OnInit {

  //ipInfo = this.iPaddressService.ipInfo
  address?: IAddress = undefined


  ipRegex = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
  maskRegex = /^([1-9]|[12][0-9]|3[012])$/


  addressForm = this.formBuilder.group({
    address: new FormControl("", [Validators.pattern(this.ipRegex), Validators.required]),
    mask: new FormControl("", [Validators.pattern(this.maskRegex), Validators.required]),
    classful: new FormControl(false, [Validators.required]),
  });
  isClassful: boolean = false;

  constructor(private formBuilder: FormBuilder) {

  }

  ngOnInit(): void {

  }


  get addressClass() {
    return this.address?.getIpDefaultClass()
  }

  get binMask() {
    return this.address?.mask?.getIpBinary()
  }

  get shortMask() {
    return this.address?.mask?.getShort()
  }

  getNbHosts() {
    /*if (this.iPaddressService.network) {
      return this.iPaddressService.network.hosts
    }*/
    if (this.address) {
      return Math.pow(2, this.address.mask!.getIpBinary().split(/0/).length - 1)
    }
    return 0
  }

  get mask(): string | null {
    return this.address?.mask?.ip!
  }

  get networkAddress() {
    return this.address?.getSuperNet().ip
  }

  get networkAddressFormatted() {
    return this.address?.getSuperNet().ip
  }


  get binary() {
    return this.address?.getIpBinary()
  }

  get inputAddress() {
    return this.addressForm.get('address')
  }

  get inputMask() {
    return this.addressForm.get("mask")!
  }

  get inputSubmask() {
    return this.addressForm.get("submask")
  }

  validate() {


    /*if (!this.iPaddressService.address) {
      this.iPaddressService.address = new Address("0.0.0.0")
    }
    if (!this.iPaddressService.network) {
      this.iPaddressService.network = new Network(this.iPaddressService.address, this.iPaddressService.address.mask!)
    }

    this.iPaddressService.address.ip = this.inputAddress?.value
    this.iPaddressService.address.mask = Address.generateMask(this.inputMask?.value)
    this.address = this.iPaddressService.address
    this.iPaddressService.network.address = this.iPaddressService.address.getSuperNet()
    this.iPaddressService.network.mask = this.iPaddressService.address.mask
    console.log(this.isClassfulInput?.value)*/
    this.isClassful = this.isClassfulInput?.value

    this.instentiateAddress()


  }

  private instentiateAddress() {
    if (this.isClassfulInput?.value) {
      this.address = new Address(this.inputAddress?.value, -1)

    } else {
      this.address = new Address(this.inputAddress?.value, this.inputMask.value)
    }
  }

  get isClassfulInput() {
    return this.addressForm.get("classful")
  }
}
